//
//  CountryClubTests.swift
//  CountryClubTests
//
//  Created by user157844 on 9/6/19.
//  Copyright © 2019 tarunv. All rights reserved.
//

import XCTest
@testable import CountryClub


class CountryClubTests: XCTestCase {

    var country: Countries!
    
    override func setUp() {       
        // Initializing the country object with the required parameters.
        country = Countries(countryName: "India", capital: "New Delhi", region: "Asia", currency: "INR", timezone: "UTC +5:30")
    }
    
    // Testing the function getCountryName() from class Countries which returns country name.
    func testGetCountryName()
    {
        // comparing the expected output with the return value of the function. If it matches, the TestCase passes else it fails.
        XCTAssertEqual("India", country.getCountryName())
    }
}
