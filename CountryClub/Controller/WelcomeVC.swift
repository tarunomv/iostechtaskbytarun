//
//  WelcomeVC.swift
//  CountryClub
//
//  Created by user157844 on 9/6/19.
//  Copyright © 2019 tarunv. All rights reserved.
//

import UIKit
import Foundation

class WelcomeVC: UIViewController {

    // outlets
    @IBOutlet weak var goBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting the background image
        let backgroundImage = UIImage.init(named: "bgImage")
        let backgroundImageView = UIImageView.init(frame: self.view.frame)
        backgroundImageView.image = backgroundImage
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.alpha = 0.7
        self.view.insertSubview(backgroundImageView, at: 0)
        
        // Setting the button design
        goBtn.layer.cornerRadius = 10
        goBtn.layer.shadowOffset = .zero
        goBtn.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        goBtn.layer.shadowRadius = 5
        goBtn.layer.shadowOpacity = 1.0
    }
}
