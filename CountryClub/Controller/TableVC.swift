//
//  TableVC.swift
//  CountryClub
//
//  Created by user157844 on 9/7/19.
//  Copyright © 2019 tarunv. All rights reserved.
//

import UIKit
import Foundation

class TableVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // outlets
    @IBOutlet weak var tableView: UITableView!
    // variables
    private var countries = [Countries]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setListener()
    }
    
    func setListener()
    {
        // setting the RapidAPI host and key as the header parameters
        let headers = [     "x-rapidapi-host": "restcountries-v1.p.rapidapi.com",
                            "x-rapidapi-key": "1d3bf9d76emsh94aea30f99e8babp175787jsn78374dbbb54a"        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://restcountries-v1.p.rapidapi.com/all")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error ?? "Some error ocurred")
            } else {
                //  let httpResponse = response as? HTTPURLResponse
                //  print(httpResponse!)
                do
                {
                    // converting the received data into Json object.
                    let json = try? JSONSerialization.jsonObject(with: data!, options: []) as AnyObject
                    self.countries = Countries.parseData(data: json as AnyObject)
                    // UITableView.reloadData() requires main thread to run, so using Asynchronous Dispatch Main Queue to execute the statement.
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            }
        })
        dataTask.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count          // this will return the number of countries, which will decide the number of rows in our tableView.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as? CountryCell
        {
            // Setting the background color of alternate table rows.
            if (indexPath.row % 2 == 0)
            {
                cell.contentView.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
            } else {
                cell.contentView.backgroundColor = #colorLiteral(red: 0.4872593736, green: 0.8106648303, blue: 0.885786802, alpha: 1)
            }
            cell.configureCell(country: countries[indexPath.row])       // this function will populate each cell according to row index.
            cell.layer.cornerRadius = 15
            cell.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.layer.borderWidth = 5
            return cell
        }
        else
        {
            return UITableViewCell()
        }
    }
  
    // The below function is used to add the Action Sheet when a user taps on a row.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.displayActionSheet(indexPath: indexPath as NSIndexPath)
    }
    
    // The function accepts row index value as parameter as displays the ActionSheet with corresponding country values.
    func displayActionSheet(indexPath: NSIndexPath)
    {
        let currencyVal: String = countries[indexPath.row].currency
        let timeVal: String = countries[indexPath.row].timezone
        
        let alertController = UIAlertController(title: "More Info", message: "Tap anywhere to go back.", preferredStyle: .actionSheet)
        let currency = UIAlertAction(title: "Currency: \(currencyVal)", style: .default, handler: { (action) -> Void in
        //   print("Functionality not added")
        })
        let timezone = UIAlertAction(title: "TimeZone: \(timeVal)", style: .default, handler: { (action) -> Void in
        //   print("Functionality not added")
        })
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            print("Cancel button tapped")
            self.tableView.setEditing(false, animated: true)
        })
        
        alertController.addAction(currency)
        alertController.addAction(timezone)
        alertController.addAction(cancelButton)
        
        // using popover when (/if) working on iPad
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alertController, animated: true, completion: nil)
    }
}
