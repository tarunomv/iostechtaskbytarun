//
//  CountryCell.swift
//  CountryClub
//
//  Created by user157844 on 9/9/19.
//  Copyright © 2019 tarunv. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell, UIActionSheetDelegate {

    // outlets
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var capitalLbl: UILabel!
    @IBOutlet weak var regionLbl: UILabel!
    
    // variables
    private var country: Countries!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // This function will populate the particular cell (called by row index) with the details fetched from Countries class.
    func configureCell(country: Countries)
    {
        self.country = country
        countryNameLbl.text = country.countryName
        capitalLbl.text = "Capital: \(country.capital! as String)"
        regionLbl.text = "Region: \(country.region! as String)"
    }
}
