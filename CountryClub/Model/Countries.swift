//
//  Countries.swift
//  CountryClub
//
//  Created by user157844 on 9/9/19.
//  Copyright © 2019 tarunv. All rights reserved.
//

import Foundation

class Countries
{
    // variables
    private(set) var countryName: String!
    private(set) var capital: String!
    private(set) var region: String!
    private(set) var currency: String!
    private(set) var timezone: String!
    
    init(countryName: String, capital: String, region: String, currency: String, timezone: String)
    {
        self.countryName = countryName
        self.capital =  capital
        self.region  = region
        self.currency = currency
        self.timezone = timezone
    }
    
    // This function takes Json object (containing all the values returned by RapidAPI) as parameter and parse the required values into the variables...
    // ..then store it into an array of type Countries, which will be used to populate each cell.
    class func parseData(data: AnyObject) -> [Countries]
    {
        var countries = [Countries]()
        let array = data as? [AnyObject]
        for i in 0 ..< array!.count
        {
            let eachCountry = array![i]
            let countryN = eachCountry["name"] as! String
            let capitalN = eachCountry["capital"] as! String
            let regionN = eachCountry["region"] as! String
            let currencies = eachCountry["currencies"] as AnyObject
            let currencyN = currencies[0] as! String
            let timezones = eachCountry["timezones"] as AnyObject
            let timeZone = timezones[0] as! String
            //      print(countryN); print(capitalN); print("\n")
                            
            let newCountry = Countries(countryName: countryN, capital: capitalN, region: regionN, currency: currencyN, timezone: timeZone)
            countries.append(newCountry)
        }
        return countries
    }
    
    // Created a simple getter method which is used to demonstrate unit test.
    func getCountryName() -> String
    {
        return self.countryName
    }
}
